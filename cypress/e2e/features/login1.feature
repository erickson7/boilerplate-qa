Feature: Login por meio de uma página na web

  Eu como <usuário>, posso <acessar a página de login do sistema, onde posso informar email e password>, de modo que <possa realizar login>

  Scenario Outline: Usuário e senha "<cenario>"
    Given acesso o sistema Desenrola
    When eu estou no login
    And preencher com "<email>" e "<senha>"
    Then eu acesso o sistema.

Examples:
    |usuário|cenario|email|senha|
    |Erickson|correta|ext.erickson.martinez@nexpe.co|Teste@1212|
