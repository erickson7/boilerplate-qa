Feature: Login por meio de uma página na web

  Eu como <usuário>, posso <acessar a página de login do sistema, onde posso informar email e password>, de modo que <possa realizar login>

  Background: Estou logado como
    Given estou logado com "ext.erickson.martinez@nexpe.co" e "Teste@1212"

  Scenario Outline: Acesso menu <menu>
    When eu clicar no "<menu>"
    And acesso o submenu "Esteira"
    Examples:
      | menu     |
      | Negócios |
