/// <reference types="Cypress" />
import {  Given, When, Then, And } from "@badeball/cypress-cucumber-preprocessor";
Given("acesso o sistema Desenrola", () => { // we can make an anonymus function as well here, use "()=>" instead of function()
  cy.visit("Account/Login?ReturnUrl=%2F")
})

When("eu estou no login", () => {
  cy.url().should("include", "Account/Login?ReturnUrl=%2F")
})

And(/^preencher com "([^"]*)" e "([^"]*)"$/, function (email, senha) {
  cy.get('#Email').type(email)
  cy.get('#Password').type(senha)
})

Then(/^eu acesso o sistema.$/, () => {
	cy.get('.btn').click()
  cy.url().should("include","https://desenrola.com.br/")
});
