/// <reference types="Cypress" />
import {  Given } from "@badeball/cypress-cucumber-preprocessor";
// have to import these, so that cypress can recognise cucumber keywords

Given("estou logado com {string} e {string}", (email, senha) => { // we can make an anonymus function as well here, use "()=>" instead of function()
  cy.login(email, senha)
})
