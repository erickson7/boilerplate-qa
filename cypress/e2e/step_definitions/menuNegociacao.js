/// <reference types="Cypress" />
import {  When, And } from "@badeball/cypress-cucumber-preprocessor";

When("eu clicar no {string}", (args1) => {
  cy.get('#sidebar-toggle > .material-icons').click()
  cy.contains(args1).click()
})

And(/^acesso o submenu "([^"]*)"$/, (args1) => {
  cy.contains(args1).click()
});
